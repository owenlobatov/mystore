-- MySQL Script for my_store_project

-- -----------------------------------------------------
-- Schema my_store
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) COLLATE utf8mb4_spanish_ci NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  CONSTRAINT `pk_category_id` PRIMARY KEY (`id`),
  CONSTRAINT `uq_category_name` UNIQUE (`name`)
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish_ci;

-- -----------------------------------------------------
-- Data for table `category`
-- -----------------------------------------------------
LOCK TABLES `category` WRITE;
INSERT INTO `category` (`name`) VALUES ( 'Frutas'), ('Verduras'), ('Enlatados');
UNLOCK TABLES;



-- -----------------------------------------------------
-- Table `product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) COLLATE utf8mb4_spanish_ci NOT NULL,
  `code` VARCHAR(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `category_id` SMALLINT(6)  NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  CONSTRAINT `pk_product_id` PRIMARY KEY (`id`),
  CONSTRAINT `fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
  ON DELETE RESTRICT
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish_ci;

-- -----------------------------------------------------
-- Data for table `product`
-- -----------------------------------------------------
LOCK TABLES `product` WRITE;
INSERT INTO `product` (`name`, `code`, `category_id`) VALUES ('Platano', '1111111111', 1), ('Manzana', '2222222222', 1), ('Lechuga', '3333333333', 2), ('Atun', '444444444', 3);
UNLOCK TABLES;



-- -----------------------------------------------------
-- Table `supplier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) COLLATE utf8mb4_spanish_ci NOT NULL,
  `phone_number` VARCHAR(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `representative_email` VARCHAR(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  CONSTRAINT `pk_supplier_id` PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish_ci;

-- -----------------------------------------------------
-- Data for table `supplier`
-- -----------------------------------------------------
LOCK TABLES `supplier` WRITE;
INSERT INTO `supplier` (`name`, `phone_number`, `representative_email`) VALUES ('Owen', '3123158923', 'owenlobatov@gmail.com'), ('Nahum', '3122708525', 'nahum.jvp@gmai.coml');
UNLOCK TABLES;



-- -----------------------------------------------------
-- Table `purchase_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order` (
  `id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `supplier_id` SMALLINT(6) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` DATE NOT NULL,
  `sended_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  CONSTRAINT `pk_purchase_order_id` PRIMARY KEY (`id`),
  CONSTRAINT `fk_purchase_order_supplier_id` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`)
  ON DELETE NO ACTION
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish_ci;

-- -----------------------------------------------------
-- Data for table `purchase_order`
-- -----------------------------------------------------
LOCK TABLES `purchase_order` WRITE;
INSERT INTO `purchase_order` (`supplier_id`, `delivery_date`) VALUES (1, '2022-07-05'), (2, '2022-07-10');
UNLOCK TABLES;



-- -----------------------------------------------------
-- Table `purchase_order_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `purchase_order_product`;
CREATE TABLE `purchase_order_product` (
  `purchase_order_id` SMALLINT(6) NOT NULL,
  `product_id` SMALLINT(6) NOT NULL,
  `quantity` SMALLINT(6) NOT NULL,
  `price` FLOAT NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  CONSTRAINT `pk_purchase_order_product_purchase_order_id_product_id` PRIMARY KEY (`purchase_order_id`, `product_id`),
  CONSTRAINT `fk_purchase_order_product_purchase_order_id` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`)
  ON DELETE NO ACTION,
  CONSTRAINT `fk_purchase_order_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
  ON DELETE NO ACTION
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish_ci;

-- -----------------------------------------------------
-- Data for table `purchase_order_product`
-- -----------------------------------------------------
LOCK TABLES `purchase_order_product` WRITE;
INSERT INTO `purchase_order_product` (`purchase_order_id`, `product_id`, `quantity`, `price`) VALUES (1, 1, 1, 5), (1, 2, 2, 5), (2, 3, 3, 5), (2, 4, 4, 5);
UNLOCK TABLES;
