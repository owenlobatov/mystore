""" Module for GENERIC RESPONSES """
import json

class Responses:
  """ Class for all generic responses """

  @staticmethod
  def success_index(data):
    """ Response for show a collection successfully """
    response = {
      'status_code': 200,
      'message': 'SUCCESS INDEX',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 200

  @staticmethod
  def success_show(data):
    """ Response for show a document succesfully """
    response = {
      'status_code': 200,
      'message': 'SUCCESS SHOW',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 200

  @staticmethod
  def success_create(data):
    """ Response for create a document """
    response = {
      'status_code': 201,
      'message': 'SUCCESSFULLY CREATED',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 201

  @staticmethod
  def success_update(data):
    """ Response for update a document """
    response = {
      'status_code': 201,
      'message': 'UPDATED SUCCESSFULLY',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 201

  @staticmethod
  def success_logical_deleted(data):
    """ Response for delete logically a document """
    response = {
      'status_code': 201,
      'message': 'LOGICALLY DELETED SUCCESSFULLY',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 201

  @staticmethod
  def success_destroy():
    """ Response for delete a document """
    response = {
      'status_code': 200,
      'message': 'DELETED SUCCESSFULLY',
      'data': None
    }
    return json.dumps(response, ensure_ascii=False), 200

  @staticmethod
  def success_send(data):
    """ Response for modify the sended date """
    response = {
      'status_code': 201,
      'message': 'SENDED SUCCESSFULLY',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 201

  @staticmethod
  def invalid_params():
    """ Response for invalid params """
    response = {
      'status_code': 400,
      'message': 'THE PARAMS SENT ARE INVALID',
      'data': None
    }
    return json.dumps(response, ensure_ascii=False), 400

  @staticmethod
  def not_found(data):
    """ Bad response for a search of a resource """
    response = {
      'status_code': 404,
      'message': 'RESOURCE WAS NOT FOUND',
      'data': data
    }
    return json.dumps(response, ensure_ascii=False), 404
