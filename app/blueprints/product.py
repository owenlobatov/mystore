""" PRODUCT Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.product_model import Product
from schemas.product_schema import ProductSchema

product_bp = Blueprint('product', __name__, url_prefix='/products')

@product_bp.get('/')
def index():
  """ Get all products """
  params = request.args
  products_list = Product().get_all(params)
  products = ProductSchema(many=True).dump(products_list)
  return Responses.success_index(products)

@product_bp.get('/<product_id>')
def show(product_id):
  """ Get a product by the given id """
  product = Product().find_by_params({'id': product_id})
  prod = ProductSchema(many=False).dump(product)
  if prod:
    return Responses.success_show(prod)
  return Responses.not_found(prod)

@product_bp.post('')
def create():
  """Create a product in DB"""
  params = json.loads(request.data)
  try:
      product = Product(**params)
      product.create()
  except OperationalError: 
      raise InvalidParamsException()
  except TypeError:
      raise InvalidParamsException()
  except DataError:
      raise InvalidParamsException()
  except IntegrityError:
      raise InvalidParamsException()
  created_prod = ProductSchema(many=False).dump(product.find_by_params(params))
  return Responses.success_create(created_prod)

@product_bp.put('/<product_id>')
def update(product_id):
  """Update the product according to the given id"""
  body = json.loads(request.data)
  updated_prod = Product().update(product_id, body)
  if updated_prod:
    return Responses.success_update(
      ProductSchema(many=False).dump(updated_prod)
    )
  return Responses.not_found({})

@product_bp.delete("/<product_id>")
def logic_delete(product_id):
  """Make a logical delete of an product"""
  deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  product = Product().update(product_id, {"deleted_at": deleted_at})
  if product:
    product = ProductSchema(many=False).dump(product)
    return Responses.success_logical_deleted(product)
  return Responses.not_found({"id":product_id})

@product_bp.delete("/<product_id>/destroy")
def destroy(product_id):
  """ Make a permanent delete of a product """
  response = Product().destroy(product_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": product_id})
