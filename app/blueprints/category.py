""" CATEGORY Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from models.product_model import Product
from schemas.product_schema import ProductSchema
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.category_model import Category
from schemas.category_schema import CategorySchema

category_bp = Blueprint('category', __name__, url_prefix='/categories')

@category_bp.get('/')
def index():
  """ Get all categories """
  params = request.args
  category_list = Category().get_all(params)
  category = CategorySchema(many=True).dump(category_list)
  return Responses.success_index(category)

@category_bp.get('/<category_id>')
def show(category_id):
  """ Get a category by the given id """
  category = Category().find_by_params({'id': category_id})
  cat = CategorySchema(many=False).dump(category)
  if cat:
    return Responses.success_show(cat)
  return Responses.not_found(cat)

@category_bp.post('')
def create():
  """Create a category in DB"""
  params = json.loads(request.data)
  try:
      category = Category(**params)
      category.create()
  except OperationalError: 
      raise InvalidParamsException()
  except TypeError:
      raise InvalidParamsException()
  except DataError:
      raise InvalidParamsException()
  except IntegrityError:
      raise InvalidParamsException()
  created_cat = CategorySchema(many=False).dump(category.find_by_params(params))
  return Responses.success_create(created_cat)

@category_bp.put('/<category_id>')
def update(category_id):
  """Update the category according to the given id"""
  body = json.loads(request.data)
  updated_cat = Category().update(category_id, body)
  if updated_cat:
    return Responses.success_update(
      CategorySchema(many=False).dump(updated_cat)
    )
  return Responses.not_found({})

@category_bp.delete("/<category_id>")
def logic_delete(category_id):
  """Make a logical delete of a category"""
  deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  category = Category().update(category_id, {"deleted_at": deleted_at})
  if category:
    category = CategorySchema(many=False).dump(category)
    return Responses.success_logical_deleted(category)
  return Responses.not_found({"id":category_id})

@category_bp.delete("/<category_id>/destroy")
def destroy(category_id):
  """ Make a permanent delete of a category """
  response = Category().destroy(category_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": category_id})

@category_bp.get('/<category_id>/products')
def show_category_products(category_id):
  """ Get all the products of a category by the given id """
  category = Category().find_by_params({'id': category_id})
  cat = CategorySchema(many=False).dump(category)
  product = Product().get_all({'category_id': category_id})
  prod = ProductSchema(many=True).dump(product)
  if cat:
    if prod:
      return Responses.success_show(prod)
    return Responses.not_found(prod)
  return Responses.not_found(cat)
