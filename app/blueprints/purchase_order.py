""" PURCHASE ORDER Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.purchase_order_model import PurchaseOrder
from schemas.purchase_order_schema import PurchaseOrderSchema
from models.purchase_order_product_model import PurchaseOrderProduct
from schemas.purchase_order_product_schema import PurchaseOrderProductSchema

purchase_order_bp = Blueprint('purchase_order', __name__, url_prefix='/purchase-orders')

@purchase_order_bp.get('/')
def index():
  """ Get all purchase_order """
  params = request.args
  purchase_order_list = PurchaseOrder().get_all(params)
  purchase_order = PurchaseOrderSchema(many=True).dump(purchase_order_list)
  return Responses.success_index(purchase_order)

@purchase_order_bp.get('/<purchase_order_id>')
def show(purchase_order_id):
  """ Get a purchase_order by the given id """
  purchase_order = PurchaseOrder().find_by_params({'id': purchase_order_id})
  po = PurchaseOrderSchema(many=False).dump(purchase_order)
  if po:
    return Responses.success_show(po)
  return Responses.not_found(po)
  
@purchase_order_bp.post('')
def create():
  """Create a purchase_order in DB"""
  params = json.loads(request.data)
  try:
      purchase_order = PurchaseOrder(**params)
      purchase_order.create()
  except OperationalError: 
      raise InvalidParamsException()
  except TypeError:
      raise InvalidParamsException()
  except DataError:
      raise InvalidParamsException()
  except IntegrityError:
      raise InvalidParamsException()
  created_po = PurchaseOrderSchema(many=False).dump(purchase_order.find_by_params(params))
  return Responses.success_create(created_po)

@purchase_order_bp.put('/<purchase_order_id>')
def update(purchase_order_id):
  """Update the purchase_order according to the given id"""
  body = json.loads(request.data)
  updated_po = PurchaseOrder().update(purchase_order_id, body)
  if updated_po:
    return Responses.success_update(
      PurchaseOrderSchema(many=False).dump(updated_po)
    )
  return Responses.not_found({})

@purchase_order_bp.delete("/<purchase_order_id>")
def logic_delete(purchase_order_id):
  """Make a logical delete of an purchase_order"""
  deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  purchase_order = PurchaseOrder().update(purchase_order_id, {"deleted_at": deleted_at})
  if purchase_order:
    purchase_order = PurchaseOrderSchema(many=False).dump(purchase_order)
    return Responses.success_logical_deleted(purchase_order)
  return Responses.not_found({"id":purchase_order_id})

@purchase_order_bp.delete("/<purchase_order_id>/destroy")
def destroy(purchase_order_id):
  """ Make a permanent delete of a purchase_order """
  response = PurchaseOrder().destroy(purchase_order_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": purchase_order_id})

@purchase_order_bp.patch("/<purchase_order_id>")
def send(purchase_order_id):
    """Change the actual status of the sended_email"""
    purchase_order = PurchaseOrder().send_mail({"id": purchase_order_id})
    if purchase_order:
        purchase_order = PurchaseOrderSchema(many=False).dump(purchase_order)
        return Responses.success_send(purchase_order)
    return Responses.not_found({"id": purchase_order_id})

""" PURCHASE-ORDER-PRODUCT """

@purchase_order_bp.get('/<purchase_order_id>/products/')
def index_purchase_order_products(purchase_order_id):
  """ Get all products by the purchase order """
  purchase_order = PurchaseOrderProduct().find_by_params({'purchase_order_id': purchase_order_id})
  po = PurchaseOrderProductSchema(many=False).dump(purchase_order)
  if po:
    products_list = PurchaseOrderProduct().get_all({'purchase_order_id': purchase_order_id})
    products = PurchaseOrderProductSchema(many=True).dump(products_list)
    return Responses.success_index(products)
  return Responses.not_found(purchase_order)

@purchase_order_bp.get('/<purchase_order_id>/products/<product_id>')
def show_purchase_order_product(purchase_order_id, product_id):
  """ Get a product by the purchase order """
  purchase_order_product = PurchaseOrderProduct().find_by_params({
    'purchase_order_id': purchase_order_id, 
    'product_id': product_id, 
    })
  pop = PurchaseOrderProductSchema(many=False).dump(purchase_order_product)
  if pop:
    return Responses.success_show(pop)
  return Responses.not_found(pop)

@purchase_order_bp.post('/<purchase_order_id>/products')
def create_purchase_order_product(purchase_order_id):
  """ Create a purchase_order_product """
  params = json.loads(request.data)
  try:
      purchase_order_product = PurchaseOrderProduct(**params)
      purchase_order_product.create()
  except OperationalError: 
      raise InvalidParamsException()
  except TypeError:
      raise InvalidParamsException()
  except DataError:
      raise InvalidParamsException()
  except IntegrityError:
      raise InvalidParamsException()
  created_pop = PurchaseOrderProductSchema(many=False).dump(purchase_order_product.find_by_params(params))
  return Responses.success_create(created_pop)

@purchase_order_bp.put('/<purchase_order_id>/products/<product_id>')
def update_purchase_order_product(purchase_order_id, product_id):
  """ Update a product by the purchase order """
  body = json.loads(request.data)

  purchase_order_product = PurchaseOrderProduct().find_by_params({
    'purchase_order_id': purchase_order_id, 
    'product_id': product_id, 
    })
  pop = PurchaseOrderProductSchema(many=False).dump(purchase_order_product)
  if pop:
    updated_po = PurchaseOrderProduct().update(purchase_order_id, product_id, body)
    if updated_po:
      return Responses.success_update(
        PurchaseOrderProductSchema(many=False).dump(updated_po)
      )
    return Responses.not_found({})
  return Responses.not_found(pop)

@purchase_order_bp.delete("/<purchase_order_id>/products/<product_id>")
def logical_delete_purchase_order_product(purchase_order_id, product_id):
  """ Make a logical delete on POP by the given id """
  deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  purchase_order_product = PurchaseOrderProduct().update(purchase_order_id, product_id, {"deleted_at": deleted_at})
  if purchase_order_product:
    purchase_order_product = PurchaseOrderSchema(many=False).dump(purchase_order_product)
    return Responses.success_logical_deleted(purchase_order_product)
  return Responses.not_found({"purchase_order_id": purchase_order_id, 'product_id': product_id})

@purchase_order_bp.delete("/<purchase_order_id>/products/<product_id>/destroy")
def destroy_purchase_order_product(purchase_order_id, product_id):
  """ Make a permanent delete on POP by the given id """
  response = PurchaseOrder().destroy(purchase_order_id, product_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"purchase_order_id": purchase_order_id, 'product_id': product_id})

@purchase_order_bp.get('/<supplier_id>/products')
def show_supplier_products(supplier_id):
  """ Get all the products of a supplier by the given id """
  supplier = PurchaseOrder().find_by_params({'supplier_id': supplier_id})
  sup = PurchaseOrderSchema(many=False).dump(supplier)
  product = PurchaseOrderProduct().get_all()
  prod = PurchaseOrderProductSchema(many=True).dump(product)
  if sup:
    if prod:
      return Responses.success_show(prod)
    return Responses.not_found(prod)
  return Responses.not_found(sup)
