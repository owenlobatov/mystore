""" SUPPLIER Module """
import json
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from datetime import datetime
from responses.generic_responses import Responses
from models.supplier_model import Supplier
from schemas.supplier_schema import SupplierSchema

supplier_bp = Blueprint('supplier', __name__, url_prefix='/suppliers')

@supplier_bp.get('/')
def index():
  """ Get all suppliers """
  params = request.args
  suppliers_list = Supplier().get_all(params)
  supplier = SupplierSchema(many=True).dump(suppliers_list)
  return Responses.success_index(supplier)

@supplier_bp.get('/<supplier_id>')
def show(supplier_id):
  """ Get a supplier by the given id """
  supplier = Supplier().find_by_params({'id': supplier_id})
  sup = SupplierSchema(many=False).dump(supplier)
  if sup:
    return Responses.success_show(sup)
  return Responses.not_found(sup)

@supplier_bp.post('')
def create():
  """Create a supplier in DB"""
  params = json.loads(request.data)
  try:
      supplier = Supplier(**params)
      supplier.create()
  except OperationalError: 
      raise InvalidParamsException()
  except TypeError:
      raise InvalidParamsException()
  except DataError:
      raise InvalidParamsException()
  except IntegrityError:
      raise InvalidParamsException()
  created_sup = SupplierSchema(many=False).dump(supplier.find_by_params(params))
  return Responses.success_create(created_sup)

@supplier_bp.put('/<supplier_id>')
def update(supplier_id):
  """Update the supplier according to the given id"""
  body = json.loads(request.data)
  updated_sup = Supplier().update(supplier_id, body)
  if updated_sup:
    return Responses.success_update(
      SupplierSchema(many=False).dump(updated_sup)
    )
  return Responses.not_found({})

@supplier_bp.delete("/<supplier_id>")
def logic_delete(supplier_id):
  """Make a logical delete of an supplier"""
  deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  supplier = Supplier().update(supplier_id, {"deleted_at": deleted_at})
  if supplier:
    supplier = SupplierSchema(many=False).dump(supplier)
    return Responses.success_logical_deleted(supplier)
  return Responses.not_found({"id": supplier_id})

@supplier_bp.delete("/<supplier_id>/destroy")
def destroy(supplier_id):
  """ Make a permanent delete of a supplier """
  response = Supplier().destroy(supplier_id)
  if response:
      return Responses.success_destroy()
  return Responses.not_found({"id": supplier_id})
