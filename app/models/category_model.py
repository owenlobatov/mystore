""" CATEGORY model  """
from db import db
from errors.error_handler import InvalidParamsException
from datetime import datetime

class Category(db.Model):
  """ Model category class """
  __tablename__  = 'category'
  id = db.Column(db.SMALLINT, primary_key= True, nullable=False)
  name = db.Column(db.String(45), nullable=False)
  created_at = db.Column(db.DateTime, nullable=False)
  updated_at = db.Column(db.DateTime, nullable=True)
  deleted_at = db.Column(db.DateTime, nullable=True)
  products = db.relationship('Product', backref='category')

  fields = [
    "id",
    "name",
    "created_at",
    "updated_at",
    "deleted_at"
  ]

  def __validate_params(self, params):
    """ Validate if sent params are valid """
    for param in params:
      if param not in self.fields:
        raise InvalidParamsException()

  def get_all(self, params=None):
    """ Get all non deleted categories """
    self.__validate_params(params)
    return self.query.filter_by(deleted_at=None, **params).all()

  def find_by_params(self, params):
    """ Get the first coincidence to the given params """
    self.__validate_params(params)
    return self.query.filter_by(**params).first()

  def create(self):
    """ Create a category using an instance of class """
    self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db.session.add(self)
    db.session.commit()

  def update(self, id, params):
    """Update an category in DB"""
    category = self.find_by_params({'id': id, 'deleted_at': None})
    if category:
      for key, value in params.items():
        setattr(category, key, value)
      db.session.commit()
      return self.find_by_params({'id': id})
    return None

  def deactive(self, id, params):
    """Logic deleted a category in DB"""
    category = self.find_by_params({'id': id, 'deleted_at': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
    if category:
      for key, value in params.items():
        setattr(category, key, value)
      db.session.commit()
      return self.find_by_params({'id': id})
    return None

  def destroy(self, category_id):
    """Destroy an category in  DB"""
    category = self.find_by_params({"id": category_id})
    if category:
      db.session.delete(category)
      db.session.commit()
      return True
    return False
