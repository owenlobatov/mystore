""" SUPPLIER model  """
from db import db
from errors.error_handler import InvalidParamsException
from datetime import datetime

class Supplier(db.Model):
  """ Model supplier class """
  __tablename__  = 'supplier'
  id = db.Column(db.SMALLINT, primary_key= True, nullable=False)
  name = db.Column(db.String(45), nullable=False)
  phone_number = db.Column(db.String(10), nullable=False)
  representative_email = db.Column(db.String(50), nullable=False)
  created_at = db.Column(db.DateTime, nullable=False)
  updated_at = db.Column(db.DateTime, nullable=True)
  deleted_at = db.Column(db.DateTime, nullable=True)
  purchase_orders = db.relationship('PurchaseOrder', backref='supplier')

  fields = [
    "id",
    "name",
    "phone_number",
    "representative_email"
    "created_at",
    "updated_at",
    "deleted_at"
  ]

  def __validate_params(self, params):
    """ Validate if sent params are valid """
    for param in params:
      if param not in self.fields:
        raise InvalidParamsException()

  def get_all(self, params=None):
    """ Get all non deleted suppliers """
    self.__validate_params(params)
    return self.query.filter_by(deleted_at=None, **params).all()

  def find_by_params(self, params):
    """ Get the first coincidence to the given params """
    self.__validate_params(params)
    return self.query.filter_by(**params).first()

  def create(self):
    """ Create a supplier using an instance of class """
    self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db.session.add(self)
    db.session.commit()

  def update(self, id, params):
    """Update an supplier order in DB"""
    supplier = self.find_by_params({'id': id, 'deleted_at': None})
    if supplier:
      for key, value in params.items():
        setattr(supplier, key, value)
      db.session.commit()
      return self.find_by_params({'id': id})
    return None

  def deactive(self, id, params):
    """Logic deleted a supplier in DB"""
    supplier = self.find_by_params({'id': id, 'deleted_at': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
    if supplier:
      for key, value in params.items():
        setattr(supplier, key, value)
      db.session.commit()
      return self.find_by_params({'id': id})
    return None

  def destroy(self, supplier_id):
    """Destroy an supplier in  DB"""
    supplier = self.find_by_params({"id": supplier_id})
    if supplier:
      db.session.delete(supplier)
      db.session.commit()
      return True
    return False
