""" PURCHASE ORDER model  """
from db import db
from errors.error_handler import InvalidParamsException
from datetime import datetime

class PurchaseOrder(db.Model):
  """ Model purchase order class """
  __tablename__  = 'purchase_order'
  id = db.Column(db.SMALLINT, primary_key= True, nullable=False)
  supplier_id = db.Column(db.SMALLINT, db.ForeignKey("supplier.id"), nullable=False)
  created_at = db.Column(db.DateTime, nullable=False)
  delivery_date = db.Column(db.Date, nullable=False)
  sended_at = db.Column(db.DateTime, nullable=True)
  updated_at = db.Column(db.DateTime, nullable=True)
  deleted_at = db.Column(db.DateTime, nullable=True)
  purchase_orders_products = db.relationship('PurchaseOrderProduct', backref='purchase_order')

  fields = [
    "id",
    "supplier_id",
    "created_at",
    "delivery_date",
    "sended_at",
    "updated_at",
    "deleted_at"
  ]

  def __validate_params(self, params):
    """ Validate if sent params are valid """
    for param in params:
      if param not in self.fields:
        raise InvalidParamsException()

  def get_all(self, params=None):
    """ Get all non deleted products """
    self.__validate_params(params)
    return self.query.filter_by(deleted_at=None, **params).all()

  def find_by_params(self, params):
    """ Get the first coincidence to the given params """
    self.__validate_params(params)
    return self.query.filter_by(**params).first()

  def create(self):
    """ Create a purchase order using an instance of class """
    self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db.session.add(self)
    db.session.commit()

  def update(self, id, params):
    """Update an purchase order in DB"""
    purchase_order = self.find_by_params({'id': id, 'deleted_at': None})
    if purchase_order:
      for key, value in params.items():
        setattr(purchase_order, key, value)
      db.session.commit()
      return self.find_by_params({'id': id})
    return None

  def deactive(self, id, params):
    """Logic deleted a purchase_order in DB"""
    purchase_order = self.find_by_params({'id': id, 'deleted_at': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
    if purchase_order:
      for key, value in params.items():
        setattr(purchase_order, key, value)
      db.session.commit()
      return self.find_by_params({'id': id})
    return None

  def destroy(self, purchase_order_id):
    """Destroy an purchase_order in  DB"""
    purchase_order = self.find_by_params({"id": purchase_order_id})
    if purchase_order:
      db.session.delete(purchase_order)
      db.session.commit()
      return True
    return False

  def send_mail(self, params):
    """Change a sended purchase order status by the given id"""
    purchase_order = self.find_by_params(params)
    if purchase_order:
      purchase_order.sended_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
      db.session.commit()
      return purchase_order
    return None
