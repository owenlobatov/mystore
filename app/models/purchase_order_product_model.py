""" PURCHASE ORDER PRODUCT model  """
from db import db
from errors.error_handler import InvalidParamsException
from datetime import datetime

class PurchaseOrderProduct(db.Model):
  """ Model purchase order product class """
  __tablename__  = 'purchase_order_product'
  purchase_order_id = db.Column(db.SMALLINT, db.ForeignKey("purchase_order.id"), primary_key= True, nullable=False)
  product_id = db.Column(db.SMALLINT, db.ForeignKey("product.id"), primary_key= True, nullable=False)
  quantity = db.Column(db.SMALLINT, nullable=False)
  price = db.Column(db.Float, nullable=False)
  created_at = db.Column(db.DateTime, nullable=False)
  updated_at = db.Column(db.DateTime, nullable=True)
  deleted_at = db.Column(db.DateTime, nullable=True)

  fields = [
    "purchase_order_id",
    "product_id",
    "quantity",
    "price",
    "created_at",
    "updated_at",
    "deleted_at"
  ]

  def __validate_params(self, params):
    """ Validate if sent params are valid """
    for param in params:
      if param not in self.fields:
        raise InvalidParamsException()

  def get_all(self, params=None):
    """ Get all non deleted purchase_order_product """
    self.__validate_params(params)
    return self.query.filter_by(deleted_at=None, **params).all()

  def find_by_params(self, params):
    """ Get the first coincidence to the given params """
    self.__validate_params(params)
    return self.query.filter_by(**params).first()

  def create(self):
    """ Create a purchase_order_product using an instance of class """
    self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db.session.add(self)
    db.session.commit()

  def update(self, purchase_order_id, product_id, params):
    """Update a purchase_order_product in DB"""
    purchase_order_product = self.find_by_params({'purchase_order_id': purchase_order_id, 'product_id': product_id, 'deleted_at': None})
    if purchase_order_product:
      for key, value in params.items():
        setattr(purchase_order_product, key, value)
      db.session.commit()
      return self.find_by_params({
        'purchase_order_id': purchase_order_id,
        'product_id': product_id
      })
    return None

  def deactive(self, purchase_order_id, product_id, params):
    """Logic deleted a purchase_order_product in DB"""
    purchase_order_product = self.find_by_params({'purchase_order_id': purchase_order_id, 'product_id': product_id, 'deleted_at': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
    if purchase_order_product:
        for key, value in params.items():
          setattr(purchase_order_product, key, value)
        db.session.commit()
        return self.find_by_params({
          'purchase_order_id': purchase_order_id,
          'product_id': product_id
          })
    return None

  def destroy(self, purchase_order_id, product_id):
    """Destroy an product in  DB"""
    purchase_order_product = self.find_by_params({"purchase_order_id": purchase_order_id, "product_id": product_id})
    if purchase_order_product:
      if purchase_order_product:
        db.session.delete(purchase_order_product)
        db.session.commit()
        return True
    return False
