""" Main flask module"""
from flask import Flask
from db import db
from config import Config
from errors.error_handler import InvalidParamsException
from responses.generic_responses import Responses
from blueprints.product import product_bp
from blueprints.category import category_bp
from blueprints.supplier import supplier_bp
from blueprints.purchase_order import purchase_order_bp

app = Flask(__name__)
app.config.from_object(Config())

""" Blueprints """
app.register_blueprint(product_bp)
app.register_blueprint(category_bp)
app.register_blueprint(supplier_bp)
app.register_blueprint(purchase_order_bp)

db.init_app(app)

@app.errorhandler(InvalidParamsException)
def error_params(error):
  return Responses.invalid_params()

@app.route('/')
def index():
    return 'API Created by: Owen Lobato!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
