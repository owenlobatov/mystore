""" Schema for CATEGORY """
from ma import ma

class CategorySchema(ma.Schema):
  """ Category Schema class """
  class Meta:
    """ Meta class for Category Schema """
    fields = (
      "id",
      "name"
    )
