""" Schema for SUPPLIER """
from ma import ma

class SupplierSchema(ma.Schema):
  """ Supplier Schema class """
  class Meta:
    """ Meta class for Supplier Schema """
    fields = (
      "id",
      "name",
      "phone_number",
      "representative_email"
    )
