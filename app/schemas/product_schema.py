""" Schema for PRODUCT """
from ma import ma

class ProductSchema(ma.Schema):
  """ Product Schema class """
  class Meta:
    """ Meta class for Product Schema """
    fields = (
      "id",
      "name",
      "code",
      "category_id"
    )
