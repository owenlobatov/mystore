""" Schema for PURCHASE_ORDER """
from ma import ma

class PurchaseOrderSchema(ma.Schema):
  """ Purchase Order Schema class """
  class Meta:
    """ Meta class for Purchase Order Schema """
    fields = (
      "id",
      "supplier_id",
      "created_at",
      "delivery_date",
      "sended_at"
    )
