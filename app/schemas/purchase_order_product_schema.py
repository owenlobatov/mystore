""" Schema for PURCHASE_ORDER_PRODUCT """
from ma import ma

class PurchaseOrderProductSchema(ma.Schema):
  """ Purchase Order Product Schema class """
  class Meta:
    """ Meta class for Purchase Order Product Schema """
    fields = (
      "purchase_order_id",
      "product_id",
      "quantity",
      "price"
    )
